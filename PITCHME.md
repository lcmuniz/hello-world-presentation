# Flux

An application architeture for React

---

### Flux Design

- Dispatcher: Manages Data Flow
- Stores: handle State & Logic
- Views: Render Data via React

---

![Flux Explained](https://facebook.github.io/flux/img/flux-simple-f8-diagram-explained-1300w.png)
